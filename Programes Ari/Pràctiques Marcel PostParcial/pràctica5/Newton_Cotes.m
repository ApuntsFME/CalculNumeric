function I = Newton_Cotes(f,a,b,n)
%% Generem x amb punts equiespaiats
x = linspace(a,b,n+1);

%% Calcul dels pesos
c = zeros(n+1,1);
for i = 1:n+1
    c(i) = (b^(i)-a^(i))/(i);
end

A = zeros(n+1,n+1);

for i = 1:n+1
    for j = 1:n+1
        A(i,j) = x(j)^(i-1);
    end
end

w = A\c;

%% Aproximacio de la integral
I = f(x)*w
end