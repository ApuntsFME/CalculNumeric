function I = Gauss_Legendre(f,a,b,ordre)

[z,w] = QuadraturaGauss(ordre);
I = 0.5*(a+b)*f(canviinv(z,a,b))'*w';
end