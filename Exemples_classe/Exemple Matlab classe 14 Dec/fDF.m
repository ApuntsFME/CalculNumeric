function dUdx=fDF(t,U)
global A F;

dUdx=A*U+F;
