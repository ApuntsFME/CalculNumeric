global A F;

a=0;b=1; ua=0; ub=0; Tfin=0.2; 
N=20;
Ax=(b-a)/N;
x=[a:Ax:b];
u0=1-2*abs(x'-0.5);
figure(1), plot(x,u0,'-o')

unos=ones(N-1,1);
A=spdiags([unos,-2*unos,unos],-1:1,N-1,N-1)*(1/(Ax^2));
F=zeros(N-1,1); F(1)=ua/Ax^2; F(end)=ub/Ax^2;

U0=u0(2:end-1,1);
%Amb ode45
% [t,U]=ode45(@fDF,[0,Tfin],U0);
% nOfTimeSteps=length(t);
% U=[ua*ones(nOfTimeSteps,1),U,ub*ones(nOfTimeSteps,1)];
% figure(2),plot(x,U,'-o')

%Amb Euler
Un=U0; U=U0;
nOfTimeSteps=10; At=Tfin/nOfTimeSteps;
for n=1:nOfTimeSteps
    Un=Un+At*(A*Un+F);
    U=[U,Un];
end
U=[ua*ones(1,nOfTimeSteps+1);U;ub*ones(1,nOfTimeSteps+1)];
figure(3),plot(x,U,'-o'), title('Euler')

r=At/Ax^2

%Euler enrera
Un=U0; U=U0;
nOfTimeSteps=10; At=Tfin/nOfTimeSteps;
K=eye(N-1)-At*A; %factoritzar...
for n=1:nOfTimeSteps
    Un=K\(Un+At*F);
    U=[U,Un];
end
U=[ua*ones(1,nOfTimeSteps+1);U;ub*ones(1,nOfTimeSteps+1)];
figure(4),plot(x,U,'-o'), title('Euler enrera')








