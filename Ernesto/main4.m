n = 20;

A = diag(linspace(-2, -2, n+1), 0) + diag(linspace(1, 1, n), -1) + diag(linspace(1, 1, n), 1);
A(1, 2) = 0;
A(1, 1) = 0;
A(n+1, n) = 2;

dx = 1/n;

F = @(x, U) dx*A*U;
a = zeros(n+1, 1);
a(1) = 1;

[xi, yi] = euler(F, a, [0, 1], n)
xi(:, 11)
yi(2, 11)

plot(xi(1, :), yi(1, :)*4/5 + 1/5*yi(2, :))

