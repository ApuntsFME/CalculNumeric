% La función puede llamarse de varias formas:
% newton_cotes(f, xi)
% newton_cotes(f, [a, b], n)
%
% - En la segunda forma, tomaremos n+1 puntos equiespaciados entre a y b
% - La cuadratura de Simpson se puede obtener llamando
%     newton_cotes(f, [a, b], 2)
%
% * I  : representa el valor de la integral
% * w  : representa los pesos tomados
%
% ATENCIÓN: xi es un vector fila y fi también es un vector fila
function [I, w] = newton_cotes(fi, xi, varargin)
x = [];
a = xi(1);
b = xi(end);
if (size(varargin) > 0)
        c = varargin(1);
        n = cell2mat(varargin(1));
        x = linspace(a, b, n+1);
else
    x = xi;
end

w = [];
n = length(x);

for i = 1:n
    yi = zeros(1, length(x));
    yi(i) = 1;
    Pi = lagrangepoly(xi, yi);
    q = polyint(Pi);
    w(i) = diff(polyval(q,[a b]));
end
I = sum(w .* fi);

end

