% - f es la función de la EDO
% - y0 el valor inicial (vecotr columna)
% - dom = [a, b] el dominio de la EDO
% - n es el número de pasos a dar
%
% * xi  : los puntos usados para el método
% * yi  : representa el valor de la funcion y en los puntos x
function [xi, yi] = euler(f, y0, dom, n)

xi = [linspace(dom(1), dom(end), n+1); linspace(0, n, n+1)];
h = (dom(end) - dom(1)) / n;
yi = [y0];
for i = 1:n
    yi(:, i+1) = yi(:, i) + h * f(xi(1, i), yi(:, i));
end
end

