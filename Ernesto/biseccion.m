function [a, niter] = biseccion(f, xini, xfi, tol)
    fxini = f(xini);
    fxfi = f(xfi);
    niter = 0;
    if (fxfi*fxini >= 0)
        throw("Puntos iniciales y finales incorrectos (no es una positivo y otro negativo)");
    end
    while (xfi - xini > tol)
        xmid = (xfi + xini) / 2;
        fxmid = f(xmid);
        if (fxmid < 0)
            if (fxini < 0)
                xini = xmid;
                fxini = fxmid;
            else
                xfi = xmid;
                fxfi = fxmid;
            end
        else
            if (fxini >= 0)
                xini = xmid;
                fxini = fxmid;
            else
                xfi = xmid;
                fxfi = fxmid;
            end
        end
        xfi - xini;
        niter = niter + 1;
    end
    a = (xfi + xini) / 2;
end