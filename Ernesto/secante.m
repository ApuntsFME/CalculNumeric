% La función puede llamarse de varias formas:
% secante(f, x0, x1, tol)
% secante(f, x0, x1, tol, niter)
% secante(f, x0, x1, tol, 'a0')
% secante(f, x0, x1, tol, 'a0c')
%
% - El parámetro niter indica el número de iteraciones máximas.
% - Utilizamos el parámetro 'a0' para indicar que la raíz está cercana al
%     cero y por lo tanto empleamos un error más estable
% - Utilizamos el parámetro 'a0c'cuando la raíz es cercana a cero y
%     queremos emplear un criterio combinado (error en x y en f)
%
% * x  : representa los valores de x probados en la primera fila y el
%     índice correspondiente en la segunda
% * r  : el error en cada iteración en la primera fila y el índice 
%     correspondiente en la segunda
function [x, r] = secante(f, x0, x1, tol, varargin)
    mniter = intmax;
    ferror = @error;
    if (size(varargin) > 0)
        c = varargin(1);
        if (isa(c{1}, 'char'))
            if (strcmp(varargin(1), 'a0'))
                ferror = @error0;
            elseif (strcmp(varargin(1), 'a0c'))
                ferror = @error0c;
            else
                throw("Paramétro no reconocido");
            end   
        else
            mniter = cell2mat(varargin(1));
        end
    end
    
    x = [x0; 0];
    
    fx1 = f(x0);
    r = [ferror(x0, x1, fx1); 0];
    x2 = x1;
    x1 = x0;
    niter = 1;
    while (r(1, niter) > tol && niter < mniter)
        x(:, niter+1) = [x2; niter];
        fx0 = fx1;
        x0 = x1;
        fx1 = f(x2);
        x1 = x2;
        
        x2 = x1 - fx1 * (x1 - x0)/ (fx1 - fx0); 
        r(:, niter+1) = [ferror(x1, x2, fx1); niter];
        
        niter = niter + 1;
    end
end

function r = error(x, x1, fx)
    r = abs(x - x1) / abs(x1);
end

function r = error0(x, x1, fx)
    r = abs(x - x1) / (1 + abs(x));
end


function r = error0c(x, x1, fx)
    r = abs(x - x1) + abs(fx);
end