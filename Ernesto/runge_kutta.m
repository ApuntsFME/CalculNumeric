% - f es la función de la EDO
% - y0 el valor inicial (vecotr columna)
% - dom = [a, b] el dominio de la EDO
% - n es el número de pasos a dar
% - Los coeficientes a, b y c son los propios de lo métodos RK.
%    La matriz a tiene la misma orientación que en la tabla de Butcher y b
%    y c son vectores fila.
%
%    Recordemos que
%         (i + 1)    i                                             
%        Y        = Y  + h ⋅ (b1 ⋅ k1 + ... + bs ⋅ ks)             
%               ⎛ i            i                                  ⎞
%        k1 = f ⎝x  + c1 ⋅ h, Y  + h ⋅ (a11 ⋅ k1 + ... + a1s ⋅ ks)⎠
%        ...                                                       
%               ⎛ i            i                                  ⎞
%        ks = f ⎝x  + cs ⋅ h, Y  + h ⋅ (as1 ⋅ k1 + ... + ass ⋅ ks)⎠
%
%    Para emplear el método Heun, se tiene que
%        a = [ 0 0     b = [0.5 0.5]   c = [0 1]
%              1 0 ]
%
%
% * xi  : los puntos usados para el método
% * yi  : representa el valor de la funcion y en los puntos x
function [xi, yi] = runge_kutta(f, y0, dom, n, a, b, c)

xi = [linspace(dom(1), dom(end), n+1); linspace(0, n, n+1)];
h = (dom(end) - dom(1)) / n;
yi = zeros(size(y0, 1), n+1);
yi(:, 1) = y0;

if (istriu(a') && ~any(diag(a)))
    k = zeros(1, size(a, 1));
    for i = 1:n
        for j = 1:size(a, 1)
            dy = 0;
            for l = 1:j-1
                dy = dy + a(j, l) * k(j);
            end
            k(j) = f(xi(1, i) + c(j)*h, yi(:, i) + h*dy);
        end
        yi(:, i+1) = yi(:, i) + h * (b * k');
    end
else
    error("Not implemented")
end

end

