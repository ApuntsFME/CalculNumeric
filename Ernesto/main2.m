format long

F = @(x) [ x(1) + x(2)        + x(3)        - 2  ;
          -x(1) + x(4)*x(2)   + x(5)*x(3)        ;
           x(1) + x(4)^2*x(2) + x(5)^2*x(3) - 2/3;
          -x(1) + x(4)^3*x(2) + x(5)^3*x(3)      ;
           x(1) + x(4)^4*x(2) + x(5)^4*x(3) - 2/5];
J = @(x) [ 1, 1     , 1     , 0            , 0            ;
          -1, x(4)  , x(5)  , x(2)         , x(3)         ;
           1, x(4)^2, x(5)^2, 2*x(4)*x(2)  , 2*x(5)*x(3)  ;
          -1, x(4)^3, x(5)^3, 3*x(4)^2*x(2), 3*x(5)^2*x(3);
           1, x(4)^4, x(5)^4, 4*x(4)^3*x(2), 4*x(5)^3*x(3) ];
       
  
[x, r] = newton_raphson(F, J, [2/3; 2/3; 2/3; 0; 1], 0.5e-10, 6)