% La función puede llamarse de varias formas:
% newton_raphson(f, J, x0, tol)
% newton_raphson(f, J, x0, tol, niter)
% newton_raphson(f, J, x0, tol, 'a0')
% newton_raphson(f, J, x0, tol, 'a0c')
%
% - El parámetro niter indica el número de iteraciones máximas.
% - Utilizamos el parámetro 'a0' para indicar que la raíz está cercana al
%     cero y por lo tanto empleamos un error más estable
% - Utilizamos el parámetro 'a0c'cuando la raíz es cercana a cero y
%     queremos emplear un criterio combinado (error en x y en f)
%
% * x  : representa los valores de x probados (primera columna 0)
% * r  : el error en cada iteración (primera columna 0)
%
% ATENCIÓN: x0 y las entradas de J y f tienen que ser un vector columna
function [x, r] = newton_raphson(f, J, x0, tol, varargin)
    mniter = intmax;
    ferror = @error;
    if (size(varargin) > 0)
        c = varargin(1);
        if (isa(c{1}, 'char'))
            if (strcmp(varargin(1), 'a0'))
                ferror = @error0;
            elseif (strcmp(varargin(1), 'a0c'))
                ferror = @error0c;
            else
                throw("Paramétro no reconocido");
            end   
        else
            mniter = cell2mat(varargin(1));
        end
    end
    
    if size(x0, 1) == 1
        x0 = x0';
    end
    
    x = [];
    r = [];
    x1 = x0;
    niter = 0;
    while (niter == 0 || (r(1, niter) > tol && niter < mniter))
        x(:, niter+1) = [x1];
        fx = f(x1);
        
        x1 = x1 - (J(x1) \ fx);
        r(:, niter+1) = [ferror(x(:, niter+1), x1, fx); niter];
        
        niter = niter + 1;
    end
end

function r = error(x, x1, fx)
    r = norm(x - x1) / norm(x1);
end

function r = error0(x, x1, fx)
    r = norm(x - x1) / (1 + norm(x));
end


function r = error0c(x, x1, fx)
    r = norm(x - x1) + norm(fx);
end