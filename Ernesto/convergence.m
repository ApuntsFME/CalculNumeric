% WARNING: la función a veces no funciona.
% Lee solo la primera del vector error
% solo devuelve el fac si order = 1
function [order, fac] = convergence(r)
    k = [];
    rk = [];
    for i = 1:size(r, 2)
        rk = [rk; r(1, i)];
        k = [k; i];
    end
    f = fit(k, log(rk), 'exp1');
    c = coeffvalues(f);
    order = round(exp(c(2)));
    fac = 0;
    if (order == 1)
        l = polyfit(k, (log(rk) - log(rk(1, 1))) ./ k, 0);
        fac = exp(l);
    end
end

