% La función puede llamarse de varias formas:
% newton(f, fprime, x0, tol)
% newton(f, fprime, x0, tol, niter)
% newton(f, fprime, x0, tol, 'a0')
% newton(f, fprime, x0, tol, 'a0c')
%
% - El parámetro niter indica el número de iteraciones máximas.
% - Utilizamos el parámetro 'a0' para indicar que la raíz está cercana al
%     cero y por lo tanto empleamos un error más estable
% - Utilizamos el parámetro 'a0c'cuando la raíz es cercana a cero y
%     queremos emplear un criterio combinado (error en x y en f)
%
% * x  : representa los valores de x probados en la primera fila y el
%     índice correspondiente en la segunda
% * r  : el error en cada iteración en la primera fila y el índice 
%     correspondiente en la segunda
function [x, r] = newton(f, fprime, x0, tol, varargin)
    mniter = intmax;
    ferror = @error;
    if (size(varargin) > 0)
        c = varargin(1);
        if (isa(c{1}, 'char'))
            if (strcmp(varargin(1), 'a0'))
                ferror = @error0;
            elseif (strcmp(varargin(1), 'a0c'))
                ferror = @error0c;
            else
                throw("Paramétro no reconocido");
            end   
        else
            mniter = cell2mat(varargin(1));
        end
    end
    
    x = [];
    r = [];
    x1 = x0;
    niter = 0;
    while (niter == 0 || (r(1, niter) > tol && niter < mniter))
        x(:, niter+1) = [x1; niter];
        fx = f(x1);
        
        x1 = x1 - fx / fprime(x1);
        r(:, niter+1) = [ferror(x(1, niter+1), x1, fx); niter];
        
        niter = niter + 1;
    end
end

function r = error(x, x1, fx)
    r = abs(x - x1) / abs(x1);
end

function r = error0(x, x1, fx)
    r = abs(x - x1) / (1 + abs(x));
end


function r = error0c(x, x1, fx)
    r = abs(x - x1) + abs(fx);
end