% - f es la función de la EDO
% - x0 el valor inicial
% - dom = [a, b] el dominio de la EDO
% - n es el número de pasos a dar
%
% * xi  : los puntos usados para el método
% * yi  : representa el valor de la funcion y en los puntos x
%
% ATENCIÓN: xi es un vector fila y f toma parámetros xi, yi
% x = [ x0 x1 ... xn
%       y0 y1 ... yn
%       z0 z1 ... zn ] (el tamaño de la columna es irrelevante)
function [xi, yi] = diferencias_centrads(f, y0, dom, n)

xi = [linspace(dom(1), dom(end), n+1); linspace(0, n, n+1)];
h = (dom(end) - dom(1)) / n;
y1 = y0 +h*f(xi(1), y0);
yi = [y0, y1];
for i = 2:n
    yi(:, i+1) = yi(:, i-1) + 2*h * f(xi(1, i), yi(i));
end
end

