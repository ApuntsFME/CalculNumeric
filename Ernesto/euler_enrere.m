% La función puede llamarse de varias formas:
% euler_enrere(f, y0, dom, n)
% euler_enrere(f, y0, dom, n, g)
%
% - La segunda versión permite dar una solución explícita para la ecuación
%      (i + 1)    i         ⎛ (i + 1)   (i + 1)⎞
%     Y        = Y  + h ⋅ f ⎝x       , Y       ⎠
%   Propia del método de euler hacia atrás, la función será
%     g(Y(i), h, x(i+1), f) = Y(i+1)
%
% - f es la función de la EDO
% - y0 el valor inicial (vecotr columna)
% - dom = [a, b] el dominio de la EDO
% - n es el número de pasos a dar
%
% * xi  : los puntos usados para el método
% * yi  : representa el valor de la funcion y en los puntos x
function [xi, yi] = euler_enrere(f, y0, dom, n, varargin)
g = @numerical_solve;
if (size(varargin) > 0)
    c = varargin(1);
    g = c{1};
end

xi = [linspace(dom(1), dom(end), n+1); linspace(0, n, n+1)];
h = (dom(end) - dom(1)) / n;
yi = [y0];
for i = 1:n
    yi(:, i+1) = g(yi(i), h, xi(1, i+1), f);
end
end

function Y1 = numerical_solve(Yi, h, x1, f)
    fun = @(y) Yi + h*f(x1, y) - y; 
    Y1 = fsolve(fun, Yi);
end

