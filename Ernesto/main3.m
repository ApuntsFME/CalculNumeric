f = @(x, y) [ y(2); y(3); -0.5*y(1)*y(3) ]

[xi, yi] = euler(f, [0; 0; 0.25], [0, 10], 100)
x = secante(@F, 0.25, 0, 0.5e-8);
b = x(1, end)
[xi, yi] = euler(f, [0; 0; b], [0, 10], 500);
yi(:, 1)
yi(:, end)

function r = F(b)
    f = @(x, y) [ y(2); y(3); -0.5*y(1)*y(3) ];
    [xi, yi] = euler(f, [0; 0; b], [0, 10], 500);
    r = yi(2, end) - 1;
end

