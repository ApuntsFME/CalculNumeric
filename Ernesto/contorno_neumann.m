%% WARINING NOT TESTED
%
% Recordemos que
%                ⎛ 2    ⎞
%    ∂u          ⎜∂  u  ⎟ 
%    ── = ν(x) ⋅ ⎜──────⎟   con x ∈ (a, b), t > 0
%    ∂t          ⎜    2 ⎟
%                ⎝  ∂x  ⎠
%
%    u(x, 0) = f(x) como condiciones iniciales y las condiciones de
%    contorno son:
%
%         du                  ∂u
%    -ν ⋅ ──(a) = qa     -ν ⋅ ──(b) = qb
%         dx                  ∂x
%
% - v es la función
% - f es la condicion inicial
% - qa y qb los las condiciones de contorno
% - dom = [a, b] es el dominio
% - n es el número de pasos a dar
%
% * Z  : los puntos usados para el método (matriz de puntos)
%           Z(i, j, 1) es el valor tij
%           Z(i, j, 2) es el valor xij
% * yi  : representa el valor de la funcion u en los puntos x
%
% nota, podemos representar los puntos con
%         mesh(Z(:, :, 1), Z(:, :, 2), yi)
function [Z, yi] = contorno_neumann(v, f, qa, qb, dom, n)


A = diag(linspace(-2, -2, n+1), 0) + diag(linspace(1, 1, n), -1) + diag(linspace(1, 1, n), 1);
A(1, 2) = 0;
A(1, 1) = 0;
A(n+1, n) = 2;
A
F = zeros(n+1, 1);
F(1) = qa;
F(n) = qb;

dx =  ( dom(1) - dom(2) )/n;

h = @(x, U) v(x)*A*U / dx^2 + F*2/dx;

xi = linspace(dom(1), dom(2), n+1);
U0 = [];
for i = 1:n+1
    U0(i, 1) = f(xi(i));
end

[ti, yi] = euler(h, U0, dom, n)

Z = [];
for i = 1:n+1
    for j = 1:n+1
        Z(i, j, :) = [ti(j), xi(i)];
    end
end

end


